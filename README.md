# Panorama

A tool to replace `<image></image>` tags in a svg by their referenced source file.

```
Usage: panorama [options]

Replace the images in a target svg with sources svgs.

Options:
  -V, --version        output the version number
  -i, --input <path>   .html or .svg file containing the <image> tags (default: "")
  -o, --output <path>  the file/folder where the result should be stored (default: "output/")
  -w, --watch          automaticaly run the script when a source file is updated (default: false)
  -h, --help           display help for command
```

> If no `source` argument is specified, the script will search in the current directory if an `.html` file exist an take it as the input. Otherwise, the default value is empty. If nothing is given and the script cannot find any suitable file, you will have the following error: `No input file was specified or found in the folder (...)`.

## Setup

Clone the repo and install the dependencies.

```
git clone https://gitlab.cern.ch/mlhoutel/panorama.git

cd panorama

npm install
```

You can run it using

```
node index.js --source=[SOURCE] --slides=[SLIDES] --output=[OUTPUT]
```

You can build the examples in `test/output` with

```
npm run test            # tests .html input/output
npm run test-svg        # tests .svg input/output
```

## Build

**Require node version >= 14**

You can bundle exe files with a `node.js` build and all dependancies with [pkg](https://github.com/vercel/pkg)

```
npm run build           # build for the current os
npm run build:all       # build for linux and windows (x64 & x86)
```

## Made with

- [node v16](https://github.com/nodejs/node)
- [commander](https://github.com/tj/commander.js/)
- [jsdom](https://github.com/jsdom/jsdom)
- [pkg](https://github.com/vercel/pkg)