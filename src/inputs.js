const { Command } = require("commander");
const path = require("path");

const { isDirectory, listFiles } = require("./system.js");

/**
 * Parse cli inputs with commander
 * @returns { input, output, watch }
 */
function parseInputs() {

  // try to find an html file in the dir for the default input
  const files = listFiles(".");
  const html = new RegExp(".*\.html", "i");
  const match = [ ...files ].find((f) => f.match(html));

  // default values
  const SOURCE_SOZI = match ? path.basename(match) : "";
  const OUTPUT_FILE = `output/`;
  const WATCH_UPDATE = false;

  // parser and arguments
  const program = new Command();

  program
  .name("panorama")
  .description("Replace the images in a target svg with sources svgs.")
  .version("1.1.0");

  program
  .option("-i, --input <path>", `.html or .svg file containing the <image> tags`, SOURCE_SOZI)
  .option("-o, --output <path>", `the file/folder where the result should be stored`, OUTPUT_FILE)
  .option("-w, --watch", `automaticaly run the script when a source file is updated`, WATCH_UPDATE)
  .parse(process.argv);

  const options = program.opts();

  // Assert the paths good encoding
  options["input"] = decodeURI(options["input"]);
  options["output"] = decodeURI(options["output"]);

  // Check for valid input file (specified or automatic detection)
  if (!options["input"]) {
    throw new Error(`No input file was specified or found in the folder (${process.cwd()}).`);
  }

  // Convert output folder to file path (based on input file name)
  if (isDirectory(options["output"])) {
    options["output"] = path.join(options["output"], path.basename(options["input"]));
  }

  // If the input targeted file is the same as the output, force
  // the user to rename it to avoid overwriting it.
  if (options["input"] === options["output"]) {
    throw new Error(`Please specify different names for input and output files (currently ${options["input"]}).`);
  }

  return options;
}

module.exports = { parseInputs };
