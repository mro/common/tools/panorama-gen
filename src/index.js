#!/usr/bin/node

const nodemon = require("nodemon");
const path = require("path");

const { parseInputs } = require("./inputs");
const { isCurrent } = require("./system");
const { replaceImages } = require("./parsing");

main();

function main() {
  try {
    const { input, output, watch } = parseInputs();

    if (watch) {
      // Run each time a svg is updated
      const outdir = path.resolve(path.dirname(output));

      // if the output dir is not the current one, we ignore it
      // otherwise, we will not update output files others than
      // the targetted one, and we will only ignore this one.
      const ignored = [ isCurrent(output) ? outdir : output ];

      nodemon({
        ext: "html,svg", // watch all .svg and .html files in the dir
        ignore: ignored, // avoid infinite reload loop when writing output
        delay: 1000, // watch all .svg and .html files in the dir
        script: __filename, // script file (in project / in binary)
        args: [ '-i', input, "-o", output ] // pass arguments
      });

      console.info(`Started listening for changes in ${process.cwd()}...\n(running '${__filename} -i ${input} -o ${output}' on .svg changes)`);

      nodemon.on('restart', (file) => {
        console.info(`\nChange detected in ${file}, run script:`);
      }).on('quit', () => {
        console.info(`Stopped listening for changes in ${process.cwd()}`);
      }).on('error', (err) => {
        console.error(err);
      });
    }
    else {
      // Run once
      replaceImages(input, output);
    }

  }
  catch (err) {
    console.error(err.message);
  }
}
