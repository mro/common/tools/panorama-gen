const { JSDOM } = require("jsdom");
const path = require("path");
const fs = require("fs");

const { copyFile, createDir, isCurrent, readFile, writeFile } = require("./system");

/**
 * Replace the <image> tags in a contained file by their source. For more details
 * on the behaviour of the process, see the {@link handleImage} function.
 *
 * @param {string} input file name for the target (file in which images should be replaced)
 * @param {string} output file name for the output (file with replaced images)
 */
function replaceImages(input, output) {

  const datas = readFile(input);

  console.info(`[PANORAMA] source opened at ${input}`);

  let svgs;
  let dom;
  try {
    dom = new JSDOM(datas);
    svgs = dom.window.document.body.querySelectorAll("svg");
  }
  catch (error) {
    throw new Error(`An error occured while trying to access the DOM of ${input} (${error}).`);
  }

  if (svgs?.length) {
    console.info(`svg components found in the file`);

    console.info(`replacing images by their svgs sources...`);
    for (const image of svgs[0].querySelectorAll("image")) {
      handleImage(input, output, image);
    }
  }

  // Write the result in a file with the input name
  createDir(output);

  if (output.includes(".html")) {
    writeFile(output, dom.serialize()); // HTML DOM
  }
  else {
    writeFile(output, svgs[0].outerHTML); // SVG DOM, no headers
  }

  console.info(`output file written in ${output}`);
}

/**
 * Replace the <image> tag in a the file by the source if of svg type.
 * Otherwise, if the source is a local ressource (images etc), copy it to
 * the output folder to assert that the relative path will still work.
 *
 * @param {string} input file name for the target (file in which images should be replaced)
 * @param {string} output file name for the output (file with replaced images)
 * @param {SVGImageElement} image svg element to be processed
 */
function handleImage(input, output, image) {
  const inputDirectory = decodeURI(path.dirname(input));
  const outputDirectory = decodeURI(path.dirname(output));

  const xlink = image.getAttributeNS("http://www.w3.org/1999/xlink", "href");
  const href = image.getAttribute("href");
  const source = decodeURI(xlink || href);
  const relative = path.join(inputDirectory, source);

  // Check if the image source is not an svg
  if (!source?.includes(".svg")) {

    // Copy the file to the output and pass if relative ressource,
    // and if the output folder is not the current one (to avoid
    // overwriting current files and trigger nodemon updates)
    if (!isCurrent(output) && fs.existsSync(relative)) {
      createDir(path.join(outputDirectory, source));
      copyFile(relative, path.join(outputDirectory, source));
      console.info(` - source is not a svg (${source}). Copy to output and skip.`);
    }
    else {
      console.info(` - source is not a svg (${source.substring(0, 20)}), skipping.`);
    }

    return; // got to next image
  }

  const values = {
    height: image.getAttribute("height"),
    width: image.getAttribute("width"),
    x: image.getAttribute("x"),
    y: image.getAttribute("y")
  };

  // replace the image element with a sub svg element
  let slide;

  try { slide = readFile(relative); }
  catch (e) {
    // The slide doesnt exist, we don"t replace and we go to the next
    console.info(` - slide not found for ${relative}, skipping.`);

    return; // go to next image
  }

  console.info(` - slide source found for ${source}...`);

  replaceContent(slide, image, values);

  console.info(`    <image> succefully replaced by source <svg>`);
}


/**
 * Replace the value of the <image> with the svg in slide.
 * Inject attributes from the image (size and position).
 *
 * @param {string} slide the svg file content
 * @param {SVGImageElement} image the image tag to replace
 * @param {{ height, width, x, y }} values the attributes to inject
 */
function replaceContent(slide, image, values) {
  let fragment;
  let root;

  try {
    fragment = JSDOM.fragment(slide);
    root = fragment.querySelectorAll("svg")?.[0];

    // insert properties
    if (values.height) { root?.setAttribute("height", values.height); }
    if (values.width) { root?.setAttribute("width", values.width); }
    if (values.x) { root?.setAttribute("x", values.x); }
    if (values.y) { root?.setAttribute("y", values.y); }

    image.parentNode.replaceChild(fragment, image);
  }
  catch (error) {
    throw new Error(`An error occured while trying to access the DOM of the ${slide} slide (${error}).`);
  }
}
module.exports = { replaceImages, handleImage, replaceContent };
